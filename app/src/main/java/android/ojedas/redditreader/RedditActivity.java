package android.ojedas.redditreader;


import android.content.Intent;
import android.net.Uri;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;

public class RedditActivity extends SingleFragmentActivity implements  ActivityCallback{

    @LayoutRes
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }

    @Override
    protected Fragment createFragment() {
        return new RedditListFragment();
    }

    @Override
    public void onPostSelected(Uri redditpostUri) {
        if(findViewById(R.id.detail_fragment_container)== null) {
            Intent intent = new Intent(getApplicationContext(), RedditWebActivity.class);
            intent.setData(redditpostUri);
            startActivity(intent);

        }
        else{
            Fragment detailFragment = RedditWebFragment.newFragment(redditpostUri.toString());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, detailFragment)
                    .commit();
        }

    }
}
