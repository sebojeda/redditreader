package android.ojedas.redditreader;



import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class RedditWebFragment extends Fragment {
    private final static String POST_URL = "1";
    private String redditUrl;

    //we create our bundle set our bundle to or fragment, and then set our fragment to our onCreate function
    public static RedditWebFragment newFragment(String redditUrl){
        Bundle arguments = new Bundle();
        arguments.putSerializable(POST_URL, redditUrl);
        RedditWebFragment fragment = new RedditWebFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    //this is created so that our code can know what the redditUrl is.
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        redditUrl = getArguments().getString(POST_URL);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_reddit_web, container, false);


        WebView webView = (WebView)layoutView.findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(redditUrl);

        return layoutView;
    }
}
